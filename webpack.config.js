const webpack = require('webpack')
const path = require("path")

module.exports = {
	context: path.resolve(__dirname),
	entry: [
		'webpack-dev-server/client?http://localhost:8080',
		'webpack/hot/only-dev-server',
		"./src/index.jsx",
 		"./src/index.html"
	],
	output: {
		filename: "app.js",
		path: path.resolve(__dirname, "dist")
	},
	devServer: {
		hot: true,
		contentBase: path.resolve(__dirname, 'dist'),
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.html$/,
				exclude: /node_modules/,
				use: ["file-loader?name=index.html"]
			},
			{
				test: /\.(jsx|js)$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			},
			{
				test: /\.styl$/, 
				exclude: /node_modules/,
				use: [
					"style-loader",
					"css-loader?modules",
					"stylus-loader"	
				]
			}
		]
	}
}