import React, {Component} from "react"
import { CSSTransitionGroup } from 'react-transition-group'

import styles from "../styles/Dialog.styl"

export default class Dialog extends Component {
    constructor(props) {
        super(props)
        this.state = this.getUpdatedState(props)
    }

    getRecipeState(id) {
        return Object.assign({
            id: -1, 
            name: "", 
            description: "", 
            author: "",
        }, this.props.recipes.find(item => item.id === id))
    }

    getValidation(state = this.state) {
        let regex = new RegExp("^[a-zA-Z0-9-,\ \.\/:\(\)]+$")
        let all = true
        let result = ["name", "description", "author"].reduce((memo, key) => {
            memo[key] = regex.test(state[key]) 
            if (memo[key] === false) all = false
            return memo
        }, {})

        return Object.assign(result, { all })
    }

    getUpdatedState(props) {
        let recipeState = this.getRecipeState(props.dialog)
        return Object.assign({ 
            edit: props.edit === true || props.dialog < 0,
            validation: this.getValidation(recipeState) 
        }, recipeState)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dialog !== nextProps.dialog) {
            this.setState(this.getUpdatedState(nextProps))
        }
    }

    onValueChange(e) {
        let newState = Object.assign(this.state, { [e.target.id]: e.target.value })
        this.setState(Object.assign(newState, { validation: this.getValidation(newState)}))
    }

    onSubmit() {
        let { id, name, description, author } = this.state
        this.props.upsertRecipe(id, { name, description, author })
    }

    renderEditor() {
        let { id, name, description, author, validation } = this.state

        return <div>
            <span className={styles.dialog__label}>Jméno receptu</span>
            <input value={name} onChange={this.onValueChange.bind(this)} id="name" className={validation.name ? styles.dialog__text : styles.dialog__text___invalid} type="text"/>

            <span className={styles.dialog__label}>Autor</span>
            <input value={author} onChange={this.onValueChange.bind(this)} id="author" className={validation.author ? styles.dialog__text : styles.dialog__text___invalid} type="text"/>

            <span className={styles.dialog__label}>Popis</span>
            <input value={description} onChange={this.onValueChange.bind(this)} id="description" className={validation.description ? styles.dialog__text : styles.dialog__text___invalid} type="text"/>

            <button onClick={this.onSubmit.bind(this)} className={this.props.working || !validation.all ? styles.dialog__button___working : styles.dialog__button}>Uložit</button>
        </div>
    }

    renderPreview() {
        let { id, name, description, author } = this.state

        return <div>
            <h1>{name}</h1>
            <h2>{author}</h2>
            <p>{description}</p>
        </div>
    }

    toggleMode() {
        if (this.state.edit) {
            // reset edit state
            this.setState(this.getRecipeState(this.props.dialog))
        }

        this.setState({ edit: !this.state.edit })
    }

    renderIcon() {
        if (this.props.dialog < 0 || !this.props.logged) return null

        let path = "M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17M12,4.5C7,4.5 2.73,7.61 1,12C2.73,16.39 7,19.5 12,19.5C17,19.5 21.27,16.39 23,12C21.27,7.61 17,4.5 12,4.5Z"

        if (!this.state.edit) {
            path = "M16.84,2.73C16.45,2.73 16.07,2.88 15.77,3.17L13.65,5.29L18.95,10.6L21.07,8.5C21.67,7.89 21.67,6.94 21.07,6.36L17.9,3.17C17.6,2.88 17.22,2.73 16.84,2.73M12.94,6L4.84,14.11L7.4,14.39L7.58,16.68L9.86,16.85L10.15,19.41L18.25,11.3M4.25,15.04L2.5,21.73L9.2,19.94L8.96,17.78L6.65,17.61L6.47,15.29"
        }

        return <div onClick={this.toggleMode.bind(this)} className={styles.dialog__tools__btn}>
            <svg viewBox="0 0 24 24"><path d={path} /></svg>
        </div>
    }

    render() {
        return <CSSTransitionGroup
            transitionName={ {
                enter: styles.dialog__enter,
                enterActive: styles.dialog__enter__active,
                leave: styles.dialog__leave,
                leaveActive: styles.dialog__leave__active,
            } }
            transitionEnterTimeout={700} transitionLeaveTimeout={700}>
            { this.props.dialog < -1 ? null : <div className={styles.dialog} onClick={this.props.onClose}>
            <div className={styles.dialog__content} onClick={(e) => e.stopPropagation()}>
                <div className={styles.dialog__tools}>
                    { this.renderIcon() }
                    <div onClick={this.props.onClose} className={styles.dialog__tools__btn}>
                        <svg viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" /></svg>
                    </div>
                </div>

                {this.state.edit ? this.renderEditor() : this.renderPreview()}
            </div>
        </div>}
        </CSSTransitionGroup>
    }
}