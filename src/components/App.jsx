import React, { Component } from "react"
import RecipeList from "../containers/RecipeList.jsx"
import Dialog from "../containers/Dialog.jsx"

import styles from "../styles/App.styl"

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = { edit: false }
    }

    onEditChange(edit) {
        this.setState({ edit })
    }

    render() {
        return <div className={styles.app}>
            <RecipeList changeEdit={this.onEditChange.bind(this)} />
            <Dialog edit={this.state.edit} />
        </div>
    }
}