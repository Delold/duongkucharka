import React, { Component } from "react"
import styles from "../styles/Filters.styl"

export default class Filters extends Component {
    constructor(props) {
        super(props)
        this.state = this.mirrorProps(props.filters)
        this.timeout = null
    }

    componentWillReceiveProps(nextProps) {
        this.setState(this.mirrorProps(nextProps.filters))
    }

    mirrorProps(filters) {
        return Object.assign({ name: "", author: "" }, filters)
    }

    onChange(e) {
        clearTimeout(this.timeout)
        this.setState({[e.target.id]: e.target.value})
        this.timeout = setTimeout(() => {
            this.props.sendFilters(this.state)   
        }, 300)
    }

    render() {
        let { name, author } = this.state

        return <div className={styles.filters}>
            <input type="text" placeholder="Název" value={name} className={styles.filters__input} id="name" onChange={this.onChange.bind(this)} />
            <input type="text" placeholder="Autor" value={author} className={styles.filters__input} id="author" onChange={this.onChange.bind(this)} />
        </div>
    }
}