import React, { Component } from "react"
import { CSSTransitionGroup } from 'react-transition-group'

import styles from "../styles/RecipeList.styl"
import filterStyles from "../styles/Filters.styl"

import Filters from "../containers/Filters.jsx"

class Recipe extends Component {

    renderControls() {
        let { id, logged, onEdit, onDelete } = this.props

        if (this.props.logged) {
            return [
                <button key="edit" className={styles.recipe__button} onClick={(e) => { e.stopPropagation(); onEdit(id) }}>
                    <span className={styles.recipe__button__label}>Upravit</span>
                    <span className={styles.recipe__button__icon}>
                        <svg viewBox="0 0 24 24">
                            <path d="M16.84,2.73C16.45,2.73 16.07,2.88 15.77,3.17L13.65,5.29L18.95,10.6L21.07,8.5C21.67,7.89 21.67,6.94 21.07,6.36L17.9,3.17C17.6,2.88 17.22,2.73 16.84,2.73M12.94,6L4.84,14.11L7.4,14.39L7.58,16.68L9.86,16.85L10.15,19.41L18.25,11.3M4.25,15.04L2.5,21.73L9.2,19.94L8.96,17.78L6.65,17.61L6.47,15.29"></path>
                        </svg>
                    </span>
                </button>,
                <button key="delete" className={styles.recipe__button} onClick={(e) => { e.stopPropagation(); onDelete(id) }}>
                    <span className={styles.recipe__button__label}>Smazat</span>
                    <span className={styles.recipe__button__icon}>
                        <svg viewBox="0 0 24 24">
                            <path d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z"></path>
                        </svg>
                    </span>
                </button>
            ]
        }

        return null
    }
    render() {
        let {id, name, description, author, onShow} = this.props
        return <div className={styles.recipe} onClick={() => onShow(id)}>
            <div className={styles.recipe__info}>
                <div className={styles.recipe__info__name}>{name}</div>
                <div className={styles.recipe__info__author}>{author}</div>
            </div>
            {this.renderControls()}
        </div>
    }
}

export default class RecipeList extends Component {
    constructor(props) {
        super(props)
        this.state = { filters: false }
    }

    onItemShow(id) {
        this.props.changeEdit(false)
        this.props.onItemSelect(id)
    }

    onItemEdit(id) {
        this.props.changeEdit(true)
        this.props.onItemSelect(id)
    }

    onFilterToggle() {
        this.setState({ filters: !this.state.filters })
    }

    render() {
        let { recipes, logged, onItemDelete, onClose, onNewItem } = this.props

        return <div className={styles.recipelist}>
            <div className={styles.recipeadd}>
                <CSSTransitionGroup
                    transitionName={ {
                        enter: styles.recipeadd__login__enter,
                        enterActive: styles.recipeadd__login__enter__active,
                        leave: styles.recipeadd__login__leave,
                        leaveActive: styles.recipeadd__login__leave__active,
                    } }
                    transitionEnterTimeout={300}
                    transitionLeaveTimeout={300}>
                    { logged ? null : <div className={styles.recipeadd__login}><div className={styles.recipeadd__button}>Získání tokenu...</div></div> }
                </CSSTransitionGroup>
                <div className={styles.recipeadd__button} onClick={onNewItem}>Nový recept</div>
                <div className={styles.recipeadd__filters} onClick={this.onFilterToggle.bind(this)}><svg viewBox="0 0 24 24"><path d="M6,13H18V11H6M3,6V8H21V6M10,18H14V16H10V18Z" /></svg></div>
            </div>
            <CSSTransitionGroup
                transitionName={ {
                    enter: filterStyles.filters__enter,
                    enterActive: filterStyles.filters__enter__active,
                    leave: filterStyles.filters__leave,
                    leaveActive: filterStyles.filters__leave__active,
                } }
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}>
            { this.state.filters ? <Filters /> : null }
            </ CSSTransitionGroup>
            { recipes.map(recipe => <Recipe onEdit={this.onItemEdit.bind(this)} logged={logged} onShow={this.onItemShow.bind(this)} onDelete={onItemDelete} key={recipe.id} {...recipe} />) }
        </div>
    }
}