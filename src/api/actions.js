export let ActionTypes = {}

//helper func
const extend = (obj) => ActionTypes = Object.assign(ActionTypes, obj)

extend({
    SET_TOKEN: "SET_TOKEN",
    SET_RECIPES: "SET_RECIPES",
    ADD_RECIPE: "ADD_RECIPE",
    EDIT_RECIPE: "EDIT_RECIPE",
    UPSERT_RECIPE: "UPSERT_RECIPE",
    DELETE_RECIPE: "DELETE_RECIPE"
})

export const setToken = (token) => ({ type: ActionTypes.SET_TOKEN, token })
export const addRecipe = (recipe) => ({ type: ActionTypes.ADD_RECIPE, recipe })
export const editRecipe = (id, recipe) => ({ type: ActionTypes.EDIT_RECIPE, id, recipe })
export const deleteRecipe = (id) => ({ type: ActionTypes.DELETE_RECIPE, id })
export const setRecipes = (recipes) => ({ type: ActionTypes.SET_RECIPES, recipes })
export const upsertRecipe = (id, recipe) => ({ type: ActionTypes.UPSERT_RECIPE, id, recipe })

// app related stuff
extend({
    SET_STATE: "SET_STATE",
    SET_ERROR: "SET_ERROR",
    SET_DIALOG: "SET_DIALOG",
    SET_FILTERS: "SET_FILTERS",
    STATUS: {
        WORKING: "WORKING",
        IDLE: "IDLE"
    }
})
export const setState = (state) => ({ type: ActionTypes.SET_STATE, state })
export const setError = (error) => ({ type: ActionTypes.SET_ERROR, error })
export const setDialog = (dialog) => ({ type: ActionTypes.SET_DIALOG, dialog })
export const setFilters = (filters) => ({ type: ActionTypes.SET_FILTERS, filters })

// saga related stuff
extend({
    API_GET_TOKEN: "API_GET_TOKEN",
    API_GET_RECIPES: "API_GET_RECIPES",
    API_POST_RECIPES: "API_POST_RECIPES",
    API_PUT_RECIPES: "API_PUT_RECIPES",
    API_DELETE_RECIPES: "API_DELETE_RECIPES",
    API_FILTER_RECIPES: "API_FILTER_RECIPES"
})

export const apiAddRecipe = (recipe) => ({ type: ActionTypes.API_POST_RECIPES, recipe })
export const apiEditRecipe = (id, recipe) => ({ type: ActionTypes.API_PUT_RECIPES, id, recipe })
export const apiDeleteRecipe = (id) => ({ type: ActionTypes.API_DELETE_RECIPES, id })
export const apiFilterRecipes = (filters) => ({ type: ActionTypes.API_FILTER_RECIPES, filters })
export const apiSetRecipes = () => ({ type: ActionTypes.API_GET_RECIPES })
export const apiSetToken = () => ({ type: ActionTypes.API_GET_TOKEN })