import { call, all, put, takeLatest, takeEvery, select } from "redux-saga/effects"
import * as Api from "./api.js"

// actions
import * as Actions from "./actions"
import { ActionTypes } from "./actions"

// selectors
import { getToken, getFilters } from "./selectors"

function* fetchToken() {
    try {
        yield put(Actions.setState(ActionTypes.STATUS.WORKING))
        
        const token = yield call(Api.GET_TOKEN)
        yield put(Actions.setToken(token))

        yield put(Actions.setState(ActionTypes.STATUS.IDLE))
    } catch(e) {
        yield put(Actions.setError(e))
    }
}

function* fetchRecipes() {
    try {
        yield put(Actions.setState(ActionTypes.STATUS.WORKING))

        const { name, author } = yield select(getFilters)
        const recipes = yield call(Api.GET_RECIPES, name, author)

        yield put(Actions.setRecipes(recipes))

        yield put(Actions.setState(ActionTypes.STATUS.IDLE))
    } catch(e) {
        yield put(Actions.setError(e))
    }
}

function* fetchAddRecipe(action) {
    try {
        yield put(Actions.setState(ActionTypes.STATUS.WORKING))
        const token = yield select(getToken)

        let { name, description, author } = action.recipe
        let newRecipe = yield call(Api.NEW_RECIPE, token, name, description, author)
        
        yield put(Actions.addRecipe(newRecipe))
        yield put(Actions.setState(ActionTypes.STATUS.IDLE))
    } catch (e) {
        yield put(Actions.setError(e))
    }
}

function* fetchEditRecipe(action) {
    try {
        yield put(Actions.setState(ActionTypes.STATUS.WORKING))
        const token = yield select(getToken)

        let { name, description, author } = action.recipe
        let newRecipe = yield call(Api.EDIT_RECIPE, token, action.id, name, description, author)
        
        yield put(Actions.editRecipe(newRecipe.id, newRecipe))
        yield put(Actions.setState(ActionTypes.STATUS.IDLE))
    } catch (e) {
        yield put(Actions.setError(e))
    }
}

function* fetchDeleteRecipe(action) {
    try {
        yield put(Actions.setState(ActionTypes.STATUS.WORKING))
        yield put(Actions.deleteRecipe(action.id))

        const token = yield select(getToken)
        yield call(Api.DELETE_RECIPE, token, action.id)
        yield put(Actions.setState(ActionTypes.STATUS.IDLE))
    } catch (e) {
        yield put(Actions.setError(e))
    }
}

function* handleUpsert(action) {
    try {
        let { id, recipe } = action

        if (id < 0) { //adding new
            yield call(fetchAddRecipe, { recipe })
            yield put(Actions.setDialog(-2))
        } else { //editing
            yield put(Actions.editRecipe(id, Object.assign(recipe, { id }))) //edit recipe in temp
            yield put(Actions.setDialog(-2)) //close dialog
            yield call(fetchEditRecipe, action) //update recipe in api
        }
    } catch(e) {
        yield put(Actions.setError(e))
    }
}

function* handleFilters(action) {
    try {
        let { filters } = action
        yield put(Actions.setFilters(filters))
        yield call(fetchRecipes)
    } catch(e) {
        yield put(Actions.setError(e))
    }
}

function* handleError(action) {
    console.error(action.error)
}

export default function* saga() {
    yield all([
        takeEvery(ActionTypes.SET_ERROR, handleError),
        takeLatest(ActionTypes.API_GET_TOKEN, fetchToken),
        takeLatest(ActionTypes.API_GET_RECIPES, fetchRecipes),
        takeLatest(ActionTypes.API_POST_RECIPES, fetchAddRecipe),
        takeLatest(ActionTypes.API_PUT_RECIPES, fetchEditRecipe),
        takeLatest(ActionTypes.API_DELETE_RECIPES, fetchDeleteRecipe),
        takeLatest(ActionTypes.API_FILTER_RECIPES, handleFilters),
        takeLatest(ActionTypes.UPSERT_RECIPE, handleUpsert)
    ])
}