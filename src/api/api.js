import "whatwg-fetch"
import URLSearchParams from "url-search-params"

const BASE_URL = "https://api-recipes.herokuapp.com"

const get = (path, params = {}) => {
    let url = BASE_URL + path
    if (Object.keys(params).filter(key => params[key].trim().length > 0).length > 0) {
        let query = new URLSearchParams()
        Object.keys(params).forEach(key => {
            query.append(key, params[key])
        })
        url += `?${query.toString()}`
    }

    return fetch(url).then(a => {
        if (a.status === 204) return { data: null, errorDescription: null }
        return a.json()
    }).then(data => {
        if (data.errorDescription) return Promise.reject(data)
        return data.data
    })
}

const send = (path, token, data, method = "POST") => {
    return fetch(BASE_URL + path, {
        method,
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    }).then(a => {
        if (a.status === 204) return { data: null, errorDescription: null }
        return a.json()
    }).then(data => {
        if (data.errorDescription) return Promise.reject(data)
        return data.data
    })
}

export function GET_TOKEN() {
    return get("/tokens/new")
}

//TODO: add filtering
export function GET_RECIPES(name = null, author = null) {
    let query = {}

    if (name && name.trim().length > 0) query["name"] = name 
    if (author && author.trim().length > 0) query["author"] = author 

    return get("/recipes", query)
}

export function NEW_RECIPE(token, name = "", description = "", author = "") {
    return send("/recipes", token, { name, description, author })
}

//TODO: validate input
export function EDIT_RECIPE(token, id, name = "", description = "", author = "") {
    return send(`/recipes/${id}`, token, { name, description, author }, "PUT")
}

//TODO: validate input
export function DELETE_RECIPE(token, id) {
    return send(`/recipes/${id}`, token, {}, "DELETE")
}