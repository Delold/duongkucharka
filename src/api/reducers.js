import { ActionTypes } from "./actions" 

const initialState = {
    token: null,
    recipes: [],
    status: ActionTypes.STATUS.IDLE,
    dialog: -2,
    filters: {
        name: "",
        author: ""
    },
    error: null,
}

const token = (state = initialState.token, action) => {
    if (action.type === ActionTypes.SET_TOKEN) return action.token
    return state
}

const addRecipe = (state = initialState.recipes, action) => {
    return [...state, action.recipe]
}

const editRecipe = (state = initialState.recipes, action) => {
    return state.map(recipe => {
        if (recipe.id === action.id) return action.recipe
        return recipe
    })
}

const deleteRecipe = (state = initialState.recipes, action) => {
    return state.filter(recipe => recipe.id !== action.id)
}

const recipes = (state = initialState.recipes, action) => {
    switch (action.type) {
        case ActionTypes.SET_RECIPES: return action.recipes
        case ActionTypes.ADD_RECIPE: return addRecipe(state, action)
        case ActionTypes.EDIT_RECIPE: return editRecipe(state, action)
        case ActionTypes.DELETE_RECIPE: return deleteRecipe(state, action)
    }

    return state
}

const status = (state = initialState.status, action) => {
    if (action.type === ActionTypes.SET_STATE) return action.state
    return state
}

const dialog = (state = initialState.dialog, action) => {
    if (action.type === ActionTypes.SET_DIALOG) return action.dialog
    return state
}

const error = (state = initialState.error, action) => {
    if (action.type === ActionTypes.SET_ERROR) return action.error
    return state
}

const filters = (state = initialState.filters, action) => {
    if (action.type === ActionTypes.SET_FILTERS) return Object.assign(state, action.filters)
    return state
}

const reducer = (state = initialState, action) => {
    return {
        status: status(state.status, action),
        token: token(state.token, action),
        recipes: recipes(state.recipes, action),
        error: error(state.error, action),
        dialog: dialog(state.dialog, action),
        filters: filters(state.filters, action)
    }
}

export default reducer