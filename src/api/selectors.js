import { ActionTypes } from "./actions"

export const getToken = (state) => state.token
export const getFilters = (state) => state.filters

export const isLogged = (state) => state.token !== null && state.token.length > 0
export const isWorking = (state) => state.status === ActionTypes.STATUS.WORKING