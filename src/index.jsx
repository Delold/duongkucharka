import "babel-polyfill"

import React, { Component } from "react"
import { render } from "react-dom"
import { Provider } from "react-redux"

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import reducer from './api/reducers'
import endpoint from './api/sagas'

import { apiSetRecipes, apiSetToken } from './api/actions'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(endpoint)

setTimeout(() => store.dispatch(apiSetToken()), 1500)

store.dispatch(apiSetRecipes())


import App from "./components/App.jsx"
render(<Provider store={store}><App /></Provider>, document.getElementById("root"))