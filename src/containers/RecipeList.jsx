import { connect } from "react-redux"
import Component from "../components/RecipeList.jsx"

import { apiEditRecipe, apiAddRecipe, apiDeleteRecipe, setDialog } from "../api/actions"
import { isLogged } from "../api/selectors"

const state = (state) => {
	return {
		recipes: state.recipes,
		logged: isLogged(state)
	}
}

const actions = (dispatch) => {
	return {
		onNewItem: () => dispatch(setDialog(-1)),
		onClose: () => dispatch(setDialog(-2)),
		onItemSelect: (id) => dispatch(setDialog(id)),
		onItemDelete: (id) => dispatch(apiDeleteRecipe(id))
	}
}

const RecipeList = connect(state, actions)(Component)
export default RecipeList