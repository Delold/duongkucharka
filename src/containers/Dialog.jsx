import { connect } from "react-redux"

import Component from "../components/Dialog.jsx"

import { setDialog, upsertRecipe, apiEditRecipe, apiAddRecipe, ActionTypes } from "../api/actions"
import { isWorking, isLogged } from "../api/selectors"

const state = (state) => {
	return {
		dialog: state.dialog,
		working: isWorking(state),
		logged: isLogged(state),
		recipes: state.recipes,
	}
}

const actions = (dispatch) => {
	return {
		upsertRecipe: (id, recipe) => dispatch(upsertRecipe(id, recipe)),
		onClose: () => dispatch(setDialog(-2))
	}
}

const Dialog = connect(state, actions)(Component)
export default Dialog