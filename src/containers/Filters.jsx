import { connect } from "react-redux"
import Component from "../components/Filters.jsx"

import { apiFilterRecipes } from "../api/actions"

const state = (state) => {
	return {
		filters: state.filters,
	}
}

const actions = (dispatch) => {
	return {
		sendFilters: (filters) => dispatch(apiFilterRecipes(filters))
	}
}

const Filters = connect(state, actions)(Component)
export default Filters