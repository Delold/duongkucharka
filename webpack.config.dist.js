const webpack = require('webpack')
const path = require("path")

module.exports = {
	context: path.resolve(__dirname),
	entry: [
		"./src/index.jsx",
 		"./src/index.html"
	],
	output: {
		filename: "app.js",
		path: path.resolve(__dirname, "dist")
	},
	module: {
		rules: [
			{
				test: /\.html$/,
				exclude: /node_modules/,
				use: ["file-loader?name=index.html"]
			},
			{
				test: /\.(jsx|js)$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			},
			{
				test: /\.styl$/, 
				exclude: /node_modules/,
				use: [
					"style-loader",
					"css-loader?modules",
					"stylus-loader"	
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production'),
			}
		}),
		new webpack.optimize.UglifyJsPlugin()
	]
}